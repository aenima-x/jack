# -*- coding: utf-8 -*-
import logging
import struct
import multiprocessing
from collections import namedtuple
import importlib
import socket

from jack import RedisConsumer

logger = logging.getLogger(__name__)

from jack.protocols.protocol import IP_PROTO_SCTP, IP_PROTO_TCP, IP_PROTO_UDP, UDP_HEADER_STRUCT, TCP_HEADER_STRUCT

ip_header_struct = '!BBHHHBBH4s4s'
tcp_header_struct = '!HHIIBBHHH'
udp_header_struct = '!HHHH'

IP_PROTO_TCP = 6                # TCP
IP_PROTO_UDP = 17               # UDP
IP_PROTO_SCTP = 132             # Stream Ctrl Transmission


Protocol = namedtuple("Protocol", ("name", "module", "filter_data", "output_queue"))


class DispatcherManager(RedisConsumer):

    def __init__(self, config):
        super(DispatcherManager, self).__init__(config)
        self.load_config(config)

    def load_config(self, config):
        self.workers_count = config["workers"]
        self.input_queue = config["input_queue"]
        self.protocols = []
        for protocol_data in config["protocols"]:
            self.protocols.append(Protocol(**protocol_data))

    def dispatch_message(self, message):
        timestamp, buffer_encoded = message.split(":")
        buffer = buffer_encoded.decode('hex')
        if len(buffer) > 20:
            v, tos, length, id, off, ttl, p, sum, src, dst = struct.unpack(ip_header_struct, buffer[:20])
            ip_data = buffer[20:]
            for protocol in self.protocols:
                f = importlib.import_module("jack.protocols.%s" % protocol.module).is_packet
                if f(ip_data, p, src, dst, protocol.filter_data):
                    self.redis_queue.put(message, protocol.output_queue)
                    break
            else:
                src_ip = socket.inet_ntoa(src)
                dst_ip = socket.inet_ntoa(dst)
                if p == IP_PROTO_UDP:
                    sport, dport, ulen, sum = struct.unpack(UDP_HEADER_STRUCT, ip_data[:8])
                    ip_info = "UDP (%s:%s -> %s:%s)" % (src_ip, sport, dst_ip, dport) 
                elif p == IP_PROTO_TCP:
                    sport, dport, seq, ack, off_x2, flags, win, sum, urp = struct.unpack(TCP_HEADER_STRUCT, ip_data[:20])
                    ip_info = "TCP (%s:%s -> %s:%s)" % (src_ip, sport, dst_ip, dport)
                elif p == IP_PROTO_SCTP:
                    ip_info = "SCTP (%s -> %s)" % (src_ip, dst_ip)
                else:
                    ip_infop = "N/A"
                logger.debug("unknown protocol: %s (%s)", ip_info, buffer_encoded)

    def get_buffer_from_queue(self):
        self.get_redis_queue()
        current_process = multiprocessing.current_process()
        logger.info("create dispatcher:%s [pid:%d]", current_process.name, current_process.pid)
        while True:
            message = self.redis_queue.get(self.input_queue)
            try:
                self.dispatch_message(message)
            except Exception as e:
                logger.exception(e)

    def run(self):
        logger.debug("creating workers")
        for i in range(self.workers_count):
            p = multiprocessing.Process(target=self.get_buffer_from_queue, args=(), name="dispatcher-%d" % i)
            p.start()
