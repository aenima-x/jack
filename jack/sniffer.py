# -*- coding: utf-8 -*-
from jack import RedisConsumer
import struct
from collections import namedtuple
import logging
import socket
import multiprocessing
from ctypes import create_string_buffer, addressof
import time

import dpkt


logger = logging.getLogger(__name__)

ETH_HEADER_STRUCT = "!6s6sH"

#ETH Types
ETH_TYPE_PUP = 0x0200           # PUP protocol
ETH_TYPE_IP = 0x0800            # IP protocol
ETH_TYPE_ARP = 0x0806           # address resolution protocol
ETH_TYPE_CDP = 0x2000           # Cisco Discovery Protocol
ETH_TYPE_DTP = 0x2004           # Cisco Dynamic Trunking Protocol
ETH_TYPE_REVARP = 0x8035                # reverse addr resolution protocol
ETH_TYPE_8021Q = 0x8100         # IEEE 802.1Q VLAN tagging
ETH_TYPE_IPX = 0x8137           # Internetwork Packet Exchange
ETH_TYPE_IP6 = 0x86DD           # IPv6 protocol
ETH_TYPE_PPP = 0x880B           # PPP
ETH_TYPE_MPLS = 0x8847          # MPLS
ETH_TYPE_MPLS_MCAST = 0x8848    # MPLS Multicast
ETH_TYPE_PPPoE_DISC = 0x8863    # PPP Over Ethernet Discovery Stage
ETH_TYPE_PPPoE = 0x8864 # PPP Over Ethernet Session Stage

# As defined in asm/socket.h
SO_ATTACH_FILTER = 26

PcapSource = namedtuple('PcapSource', ['source_type', 'name', 'path'])
InterfaceSource = namedtuple('InterfaceSource', ['source_type', 'name', 'device', 'filter'])

# Source Types
PCAP_SOURCE_TYPE = 'pcap'
INTERFACE_SOURCE_TYPE = 'interface'

#
REDIS_MESSAGE_FORMAT = "%f:%s"

class SnifferManager(RedisConsumer):

    def __init__(self, config):
        super(SnifferManager, self).__init__(config)
        self.sources = []
        self.load_config(config)

    def load_config(self, config):
        sources_data = config.get("sources", None)
        if sources_data:
            for source_data in sources_data:
                source_types = {PCAP_SOURCE_TYPE: PcapSource, INTERFACE_SOURCE_TYPE: InterfaceSource}
                source = source_types.get(source_data['source_type'])(**source_data)
                self.sources.append(source)
        else:
            raise Exception("Missing Sources")

    @staticmethod
    def filter_buffer(buffer):
        header = buffer[:14]
        payload = buffer[14:]
        destination, source, protocol = struct.unpack(ETH_HEADER_STRUCT, header)
        if protocol == ETH_TYPE_8021Q:
            tag, protocol = struct.unpack('>HH', payload[:4])
            payload = payload[4:]
        if protocol == ETH_TYPE_IP:
            return payload
        else:
            return None

    def sniff_from_source(self, source):
        self.get_redis_queue()
        current_process = multiprocessing.current_process()
        logger.info("crete sniffer:%s [pid: %d]", current_process.name, current_process.pid)
        if source.source_type == PCAP_SOURCE_TYPE:
            pcap = dpkt.pcap.Reader(open(source.path))
            for ts, buffer in pcap:
                payload = self.filter_buffer(buffer)
                if payload:
                    redis_message = REDIS_MESSAGE_FORMAT % (time.time(), payload.encode('hex'))
                    self.redis_queue.put(redis_message)
            logger.debug("no more packets in pcap [%s]", source.path)
        elif source.source_type == INTERFACE_SOURCE_TYPE:
            s = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, 132)
            if source.filter:
                logger.debug("Sniffer Filter: %s", source.filter)
                blob = create_string_buffer(''.join(struct.pack("HBBI", *e) for e in source.filter))
                address = addressof(blob)
                socket_filter = struct.pack('HL', len(source.filter), address)
                s.setsockopt(socket.SOL_SOCKET, SO_ATTACH_FILTER, socket_filter)
            else:
                logger.debug("Sniff Filter: None")
                s.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, 2 ** 30)
            s.bind((source.device, 3))
            while True:
                try:
                    buffer = s.recvfrom(65565)[0]
                    payload = self.filter_buffer(buffer)
                    if payload:
                        redis_message = REDIS_MESSAGE_FORMAT % (time.time(), payload.encode('hex'))
                        self.redis_queue.put(redis_message)
                except Exception as e:
                    logger.error(e)

    def run(self):
        for source in self.sources:
            p = multiprocessing.Process(target=self.sniff_from_source, args=(source,), name="sniffer-%s" % source.name)
            p.start()


