# -*- coding: utf-8 -*-
import logging

from jack.utils import RedisQueue, RedisConfig

logger = logging.getLogger(__name__)


class RedisConsumer(object):

    def __init__(self, config):
        self.redis_config_data = config["redis"]
        self.redis_queue = None

    def get_redis_queue(self):
        logger.debug("get redis queue %s", self.redis_config_data)
        redis_config = RedisConfig(**self.redis_config_data)
        self.redis_queue = RedisQueue(host=redis_config.host, port=redis_config.port, name=redis_config.name,
                                      multiple_keys=redis_config.multiple_keys)