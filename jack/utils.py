# -*- coding: utf-8 -*-
import logging
from collections import namedtuple

import redis


# Redis


RedisConfig = namedtuple('RedisConfig', ('host', 'port', 'name', 'multiple_keys'))
RedisCacheConfig = namedtuple('RedisCacheConfig', ('host', 'port', 'expiration'))
DBConfig = namedtuple('DatabaseConfig', ('host', 'port', 'user', 'password', 'database'))

class RedisQueue(object):
    """Simple Queue with Redis Backend"""

    def __init__(self, host="localhost", port=6379, name=None, namespace='jack', multiple_keys=False):
        """The default connection parameters are: host='localhost', port=6379, db=0"""
        if not multiple_keys and not name:
            raise Exception("If you don't use multiple keys, you must provide a name")
        elif name and not multiple_keys:
            self.key = '%s:%s' % (namespace, name)
        elif not name and multiple_keys:
            self.key = None
        self.namespace = namespace
        self.db = redis.Redis(host, port)

    def get_key(self, name=None):
        if self.key:
            return self.key
        else:
            if name:
                return '%s:%s' % (self.namespace, name)
            else:
                raise Exception("Must indicate name")

    def qsize(self, name=None):
        """Return the approximate size of the queue."""
        return self.db.llen(self.get_key(name))

    def empty(self):
        """Return True if the queue is empty, False otherwise."""
        return self.qsize() == 0

    def put(self, item, name=None):
        """Put item into the queue."""
        self.db.rpush(self.get_key(name), item)

    def get(self, name=None, block=True, timeout=None):
        """Remove and return an item from the queue.

        If optional args block is true and timeout is None (the default), block
        if necessary until an item is available."""
        if block:
            item = self.db.blpop(self.get_key(name), timeout=timeout)
        else:
            item = self.db.lpop(self.get_key(name))

        if item:
            item = item[1]
        return item

    def get_nowait(self):
        """Equivalent to get(False)."""
        return self.get(False)


class RedisCache(object):

    def __init__(self, host="localhost", port=6379, expiration=2):
        """The default connection parameters are: host='localhost', port=6379, db=0"""
        self.expiration = expiration
        self.db = redis.Redis(host, port)

    def set(self, name, value):
        self.db.setex(name, value, self.expiration)

    def get(self, name):
        self.db.get(name)

    def rename(self, name, new_name):
        self.db.rename(name, new_name)

    def delete(self, names):
        self.db.delete(names)
