# -*- coding: utf-8 -*-
import logging
import socket
from datetime import datetime

import dpkt

from jack.protocols.protocol import Protocol, IP_PROTO_SCTP

logger = logging.getLogger(__name__)


# Filter Function


def is_packet(ip_data, protocol, src, dst, filter_data):
    return protocol == IP_PROTO_SCTP

# Camel Classes


class Component(object):
    def __init__(self, tcap_packet, camel_packet):
        self.timestamp = datetime.fromtimestamp(tcap_packet.timestamp)
        self.source_ip = tcap_packet.source_ip
        self.destination_ip = tcap_packet.destination_ip

class InitialDP(Component):
    TABLE = "camel_initialdp"

    def __init__(self, tcap_packet, camel_packet):
        super(InitialDP, self).__init__(tcap_packet, camel_packet)
        if hasattr(tcap_packet.primitive, 'origination_transaction_id'):
            self.origin_transaction_id = tcap_packet.primitive.origination_transaction_id
        else:
            self.origin_transaction_id = None
        if hasattr(tcap_packet, 'calling_party_gt'):
            self.calling_party_gt = tcap_packet.calling_party_gt
        else:
            self.calling_party_gt = None
        if hasattr(tcap_packet, 'called_party_gt'):
            self.called_party_gt = tcap_packet.called_party_gt
        else:
            self.called_party_gt = None
        if hasattr(camel_packet, 'calling_party_number'):
            self.calling_party_number = camel_packet.calling_party_number.digits
        else:
            self.calling_party_number = None
        if hasattr(camel_packet, 'called_party_number'):
            self.called_party_number = camel_packet.called_party_number.digits
        else:
            if hasattr(camel_packet, 'bcd_address'):
                self.called_party_number = camel_packet.bcd_address.digits
            else:
                self.called_party_number = None
        if hasattr(camel_packet, 'service_key'):
            self.service_key = camel_packet.service_key
        else:
            self.service_key = None
        if hasattr(camel_packet, 'cellGlobalId'):
            self.cellGlobalId = camel_packet.cellGlobalId
        else:
            self.cellGlobalId = None

    def get_tuple(self):
        called_party_number = self.called_party_number
        if called_party_number:
            called_party_number = called_party_number[:30]
        packet_tuple = (self.timestamp, self.source_ip, self.destination_ip, self.origin_transaction_id,
                        self.calling_party_gt, self.called_party_gt, self.calling_party_number, called_party_number,
                        self.service_key, self.cellGlobalId)
        return self.TABLE, packet_tuple


class Connect(Component):
    TABLE = "camel_connect"

    def __init__(self, tcap_packet, camel_packet):
        super(Connect, self).__init__(tcap_packet, camel_packet)
        if hasattr(tcap_packet.primitive, 'origination_transaction_id'):
            self.origin_transaction_id = tcap_packet.primitive.origination_transaction_id
        else:
            self.origin_transaction_id = None
        if hasattr(tcap_packet.primitive, 'destination_transaction_id'):
            self.destination_transaction_id = tcap_packet.primitive.destination_transaction_id
        else:
            self.destination_transaction_id = None
        if hasattr(tcap_packet, 'calling_party_gt'):
            self.calling_party_gt = tcap_packet.calling_party_gt
        else:
            self.calling_party_gt = None
        if hasattr(tcap_packet, 'called_party_gt'):
            self.called_party_gt = tcap_packet.called_party_gt
        else:
            self.called_party_gt = None
        if hasattr(camel_packet, 'called_party_number'):
            self.called_party_number = camel_packet.called_party_number.digits
        else:
            self.called_party_number = None

    def get_tuple(self):
        called_party_number = self.called_party_number
        if called_party_number:
            called_party_number = called_party_number[:30]
        packet_tuple = (self.timestamp, self.source_ip, self.destination_ip, self.origin_transaction_id,
                        self.destination_transaction_id, self.calling_party_gt, self.called_party_gt,
                        called_party_number)
        return self.TABLE, packet_tuple


class ReleaseCall(Component):
    TABLE = "camel_releasecall"

    def __init__(self, tcap_packet, camel_packet):
        super(ReleaseCall, self).__init__(tcap_packet, camel_packet)
        if hasattr(tcap_packet.primitive, 'destination_transaction_id'):
            self.destination_transaction_id = tcap_packet.primitive.destination_transaction_id
        else:
            self.destination_transaction_id = None
        if hasattr(tcap_packet, 'calling_party_gt'):
            self.calling_party_gt = tcap_packet.calling_party_gt
        else:
            self.calling_party_gt = None
        if hasattr(tcap_packet, 'called_party_gt'):
            self.called_party_gt = tcap_packet.called_party_gt
        else:
            self.called_party_gt = None
        if hasattr(camel_packet, 'cause_location'):
            self.cause_location = camel_packet.cause_location
        else:
            self.cause_location = None
        if hasattr(camel_packet, 'cause_indicator'):
            self.cause_indicator = camel_packet.cause_indicator
        else:
            self.cause_indicator = None

    def get_tuple(self):
        packet_tuple = (self.timestamp, self.source_ip, self.destination_ip, self.destination_transaction_id,
                        self.calling_party_gt, self.called_party_gt, self.cause_location,
                        self.cause_indicator)
        return self.TABLE, packet_tuple


class EventReportBCSM(Component):
    TABLE = "camel_eventreportbcsm"

    def __init__(self, tcap_packet, camel_packet):
        super(EventReportBCSM, self).__init__(tcap_packet, camel_packet)
        if hasattr(tcap_packet.primitive, 'origination_transaction_id'):
            self.origin_transaction_id = tcap_packet.primitive.origination_transaction_id
        else:
            self.origin_transaction_id = None
        if hasattr(tcap_packet.primitive, 'destination_transaction_id'):
            self.destination_transaction_id = tcap_packet.primitive.destination_transaction_id
        else:
            self.destination_transaction_id = None
        if hasattr(tcap_packet, 'calling_party_gt'):
            self.calling_party_gt = tcap_packet.calling_party_gt
        else:
            self.calling_party_gt = None
        if hasattr(tcap_packet, 'called_party_gt'):
            self.called_party_gt = tcap_packet.called_party_gt
        else:
            self.called_party_gt = None
        if hasattr(camel_packet, 'event_type'):
            self.event_type = camel_packet.event_type
        else:
            self.event_type = None

    def get_tuple(self):
        packet_tuple = (self.timestamp, self.source_ip, self.destination_ip, self.origin_transaction_id,
                        self.destination_transaction_id, self.calling_party_gt, self.called_party_gt,
                        self.event_type)
        return self.TABLE, packet_tuple


class Continue(Component):
    TABLE = "camel_continue"

    def __init__(self, tcap_packet, camel_packet):
        super(Continue, self).__init__(tcap_packet, camel_packet)
        if hasattr(tcap_packet.primitive, 'origination_transaction_id'):
            self.origin_transaction_id = tcap_packet.primitive.origination_transaction_id
        else:
            self.origin_transaction_id = None
        if hasattr(tcap_packet.primitive, 'destination_transaction_id'):
            self.destination_transaction_id = tcap_packet.primitive.destination_transaction_id
        else:
            self.destination_transaction_id = None
        if hasattr(tcap_packet, 'calling_party_gt'):
            self.calling_party_gt = tcap_packet.calling_party_gt
        else:
            self.calling_party_gt = None
        if hasattr(tcap_packet, 'called_party_gt'):
            self.called_party_gt = tcap_packet.called_party_gt
        else:
            self.called_party_gt = None

    def get_tuple(self):
        packet_tuple = (self.timestamp, self.source_ip, self.destination_ip, self.origin_transaction_id,
                        self.destination_transaction_id, self.calling_party_gt, self.called_party_gt)
        return self.TABLE, packet_tuple


# Protocol

CAMEL_CLASSES = {dpkt.camel.INITIAL_DP: InitialDP,
                 dpkt.camel.CONNECT: Connect,
                 #dpkt.camel.RELEASE_CALL: ReleaseCall,
                 #dpkt.camel.EVENT_REPORT_BCSM: EventReportBCSM,
                 dpkt.camel.CONTINUE: Continue}


class Camel(Protocol):

    def packet_to_tuple(self, packet):
        tcap_packet, component = packet
        camel_object = CAMEL_CLASSES[component.data.opcode](tcap_packet, component.data.operation)
        return camel_object.get_tuple()

    def decode_ip(self, ip, timestamp):
        try:
            for chunk in ip.sctp.data:
                if chunk.type == dpkt.sctp.DATA:
                    data_chunk = chunk.data
                    if data_chunk.payload_protocol_identifier == dpkt.sctp.M3UA:
                        m3 = dpkt.m3ua.M3UA(data_chunk.data)
                        if m3.message_class == 1:
                            try:
                                source_ip = socket.inet_ntoa(ip.src)
                                destination_ip = socket.inet_ntoa(ip.dst)
                                sccp_packet = dpkt.sccp.SCCP(m3.data)
                                tcap_packet = dpkt.tcap.TCAP(sccp_packet.tcap_data)
                                tcap_packet.calling_party_gt = sccp_packet.calling_party_address.data.digits
                                tcap_packet.called_party_gt = sccp_packet.called_party_address.data.digits
                                tcap_packet.timestamp = float(timestamp)
                                tcap_packet.source_ip = source_ip
                                tcap_packet.destination_ip = destination_ip
                                if tcap_packet.primitive.components:
                                    for component in filter(lambda x: x, tcap_packet.primitive.components):
                                        if component.data.opcode in CAMEL_CLASSES:
                                            yield self.packet_to_tuple((tcap_packet, component))
                            except Exception as e:
                                logger.error("Decode: %s", e)
                                logger.exception(e)
        except Exception as e:
            logger.error("Decode: Unknown Exception [%s]", e)
