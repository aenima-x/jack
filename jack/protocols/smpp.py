# -*- coding: utf-8 -*-
import logging
import struct
import socket
import dpkt
from datetime import datetime

from jack.protocols.protocol import Protocol, ProtocolWithSegmentation, IP_PROTO_TCP, TCP_HEADER_STRUCT

logger = logging.getLogger(__name__)


# SMPP Commands
SUBMIT_SM = 'submit_sm'
SUBMIT_SM_RESP = 'submit_sm_resp'
DELIVER_SM = 'deliver_sm'
DELIVER_SM_RESP = 'deliver_sm_resp'
ENQUIRE_LINK = 'enquire_link'
ENQUIRE_LINK_RESP = 'enquire_link_resp'
UNBIND = 'unbind'
UNBIND_RESP = 'unbind_resp'
BIND_RECEIVER = 'bind_receiver'
BIND_RECEIVER_RESP = 'bind_receiver_resp'
BIND_TRANSMITTER = 'bind_transmitter'
BIND_TRANSMITTER_RESP = 'bind_transmitter_resp'
BIND_TRANSCEIVER = 'bind_transceiver'
BIND_TRANSCEIVER_RESP = 'bind_transceiver_resp'

BIND_COMMANDS = (BIND_RECEIVER, BIND_TRANSMITTER, BIND_TRANSCEIVER)
BIND_RESP_COMMANDS = (BIND_RECEIVER_RESP, BIND_TRANSMITTER_RESP, BIND_TRANSCEIVER_RESP)

BIND_MASK = 'bind'
BIND_RESP_MASK = '_resp'


ALLOWED_COMMANDS = [SUBMIT_SM, SUBMIT_SM_RESP, ENQUIRE_LINK, ENQUIRE_LINK_RESP, UNBIND, UNBIND_RESP, BIND_RECEIVER,
                    BIND_RECEIVER_RESP, BIND_TRANSMITTER, BIND_TRANSMITTER_RESP, BIND_TRANSCEIVER,
                    BIND_TRANSCEIVER_RESP, DELIVER_SM, DELIVER_SM_RESP]


def is_packet(ip_data, protocol, src, dst, filter_data):
    if protocol == IP_PROTO_TCP:
        protocol_ips = filter_data.get("ips", None)
        if protocol_ips:  # this protocol must match one of these ips
            for ip_address in (socket.inet_ntoa(src), socket.inet_ntoa(dst)):
                if ip_address in protocol_ips:
                    return True
    return False


class Smpp(Protocol):

    def packet_to_tuple(self, packet):
        timestamp = datetime.fromtimestamp(packet.timestamp)
        if packet.command == SUBMIT_SM:  # Submit_sm
            if packet.source_addr and packet.destination_addr:
                table = "smpp_submit_sm"
                # timestamp
                packet_tuple = (timestamp, packet.source_ip, packet.destination_ip, packet.source_port,
                                packet.destination_port, packet.source_addr, packet.destination_addr,
                                packet.sequence)
            else:
                return None
        elif packet.command == SUBMIT_SM_RESP:  # Submit_sm_resp
            table = "smpp_submit_sm_resp"
            packet_tuple = (timestamp, packet.source_ip, packet.destination_ip, packet.source_port,
                            packet.destination_port, packet.sequence, packet.status)
        elif packet.command == ENQUIRE_LINK:  # enquire_link
            table = "smpp_enquire_link"
            packet_tuple = (timestamp, packet.source_ip, packet.destination_ip, packet.source_port,
                            packet.destination_port, packet.sequence)
        elif packet.command == ENQUIRE_LINK_RESP:  # enquire_link_esp
            table = "smpp_enquire_link_resp"
            packet_tuple = (timestamp, packet.source_ip, packet.destination_ip, packet.source_port,
                            packet.destination_port, packet.sequence, packet.status)
        elif packet.command in BIND_COMMANDS:  # Binds
            table = "smpp_bind"
            packet_tuple = (timestamp, packet.source_ip, packet.destination_ip, packet.source_port,
                            packet.destination_port, packet.sequence, packet.command, packet.system_id)
        elif packet.command in BIND_RESP_COMMANDS:  # Binds Resp
            table = "smpp_bind_resp"
            packet_tuple = (timestamp, packet.source_ip, packet.destination_ip, packet.source_port,
                            packet.destination_port, packet.sequence, packet.command, packet.status)
        elif packet.command == UNBIND:  # Unbind
            table = "smpp_unbind"
            packet_tuple = (timestamp, packet.source_ip, packet.destination_ip, packet.source_port,
                            packet.destination_port, packet.sequence)
        elif packet.command == UNBIND_RESP:  # unbind_resp
            table = "smpp_unbind_resp"
            packet_tuple = (timestamp, packet.source_ip, packet.destination_ip, packet.source_port,
                            packet.destination_port, packet.sequence, packet.status)
        elif packet.command == DELIVER_SM:  # deliver_sm
            table = "smpp_deliver_sm"
            packet_tuple = (timestamp, packet.source_ip, packet.destination_ip, packet.source_port,
                            packet.destination_port, packet.source_addr, packet.destination_addr, packet.sequence)
        elif packet.command == DELIVER_SM_RESP:  # deliver_sm_resp
            table = "smpp_deliver_sm_resp"
            if packet.status == 4294967295:
                packet.status = -1
                logger.error(packet)
            packet_tuple = (timestamp, packet.source_ip, packet.destination_ip, packet.source_port,
                            packet.destination_port, packet.sequence, packet.status)
        return table, packet_tuple

    def decode_ip(self, ip, timestamp):
        try:
            tcp = ip.data
            try:
                for smpp in dpkt.smpp.get_smpp(tcp.data):
                    smpp.timestamp = float(timestamp)
                    smpp.source_ip = socket.inet_ntoa(ip.src)
                    smpp.destination_ip = socket.inet_ntoa(ip.dst)
                    smpp.source_port = tcp.sport
                    smpp.destination_port = tcp.dport
                    if smpp.command in ALLOWED_COMMANDS:
                        yield self.packet_to_tuple(smpp)
            except Exception as e:
                logger.error("Decode: %s (%s)", e, tcp.data.encode('hex'))
        except Exception as e:
            logger.error("Decode: Unknown Exception [%s]", e)
