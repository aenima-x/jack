# -*- coding: utf-8 -*-
import logging
import multiprocessing
from collections import deque, defaultdict
import threading

import dpkt
from mysql.connector import MySQLConnection, errors

from jack.utils import RedisQueue, RedisCache

logger = logging.getLogger(__name__)

# IP Protocols

IP_PROTO_TCP = 6                # TCP
IP_PROTO_UDP = 17               # UDP
IP_PROTO_SCTP = 132             # Stream Ctrl Transmission

IP_HEADER_STRUCT = '!BBHHHBBH4s4s'
TCP_HEADER_STRUCT = '!HHIIBBHHH'
UDP_HEADER_STRUCT = '!HHHH'


class Protocol(object):

    def __init__(self, protocol_config):
        self.protocol_config = protocol_config
        logger.info("create decoder:%s [pid:%s] [buffer_size:%d]", protocol_config.name, multiprocessing.current_process().pid, protocol_config.buffer_size)
        self.buffer = deque(maxlen=protocol_config.buffer_size)

    def load_config(self):
        self.db_config = {'host': self.protocol_config.db_config.host,
                     'port': self.protocol_config.db_config.port,
                     'user': self.protocol_config.db_config.user,
                     'password': self.protocol_config.db_config.password,
                     'database': self.protocol_config.db_config.database}
        logger.debug("connect to redis input_queue (%s)", self.protocol_config.input_queue)
        self.redis_input_queue = RedisQueue(host=self.protocol_config.redis_input_config.host,
                                            port=self.protocol_config.redis_input_config.port,
                                            name=self.protocol_config.input_queue,
                                            multiple_keys=self.protocol_config.redis_input_config.multiple_keys)

    def packet_to_tuple(self, packet):
        raise NotImplementedError

    def decode_ip(self, ip, timestamp):
        raise NotImplementedError

    def run(self):
        self.load_config()
        logger.debug("reading packets")
        while True:
            # get packet from queue
            message = self.redis_input_queue.get()
            try:
                timestamp, hex_buffer = message.split(":")
                buffer = hex_buffer.decode('hex')
                ip = dpkt.ip.IP(buffer)
                for packet in self.decode_ip(ip, timestamp):
                    if packet:
                        if len(self.buffer) == self.protocol_config.buffer_size:
                            t = threading.Thread(target=self.save, args=(deque(self.buffer),))
                            t.start()
                            self.buffer.clear()
                        self.buffer.append(packet)
            except Exception as e:
                logger.error("%s", e)

    def save(self, packet_list):
        try:
            data_to_insert = defaultdict(list)
            for table, data in packet_list:
                data_to_insert[table].append(data)
            conn = MySQLConnection(**self.db_config)
            cursor = conn.cursor()
        except Exception as e:
            logger.error(e)
        else:
            save_stats = {}
            for table, packets in data_to_insert.items():
                try:
                    save_stats[table] = len(packets)
                    query = "INSERT INTO {0} VALUES ({1})".format(table, ",".join(['%s' for i in range(len(packets[0]))]))
                    cursor.executemany(query, packets)
                    conn.commit()
                except errors.DataError as e:
                    row_number_string = e.msg.split()[-1]
                    if row_number_string.isdigit():
                        row_number = int(row_number_string)
                        row = packets[row_number - 1] 
                        logger.error("Table: %s - %s - %s", table, e, row)
                    else:
                        logger.error("Table: %s - %s", table, e)
                except Exception as e:
                    logger.error("Table: %s - %s", table, e)
            logger.debug("save stats:[total=%d - %s", len(packet_list), save_stats)
            conn.close()



class ProtocolWithSegmentation(Protocol):

    def __init__(self, protocol_config):
        super(ProtocolWithSegmentation, self).__init__(protocol_config)
        self.radis_cache = None

    def load_config(self):
        super(ProtocolWithSegmentation, self).load_config()
        logger.debug("Connect to redis cache (%s)", self.protocol_config.redis_cache_config)
        self.redis_cache = RedisCache(host=self.protocol_config.redis_cache_config.host,
                                      port=self.protocol_config.redis_cache_config.port,
                                      expiration=self.protocol_config.redis_cache_config.expiration)
