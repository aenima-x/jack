# -*- coding: utf-8 -*-
import struct
import logging
import random
import socket
import pickle
import time
from datetime import datetime
import dpkt

from jack.protocols.protocol import Protocol, ProtocolWithSegmentation, IP_PROTO_TCP, TCP_HEADER_STRUCT

logger = logging.getLogger(__name__)

CREDIT_CONTROL_COMMAND = 272
WATCH_DOG_COMMAND = 280
RE_AUTH_COMMAND = 258
DIAMETER_DATA_MAX_SIZE = 2000
DIAMETER_HEADERS = 'B3sB3s'


# Filter Function

def is_packet(ip_data, protocol, src, dst, filter_data):
    if protocol == IP_PROTO_TCP:
        sport, dport, seq, ack, off_x2, flags, win, sum, urp = struct.unpack(TCP_HEADER_STRUCT, ip_data[:20])
        return filter_data["port"] in (sport, dport)
    else:
        return False

class Diameter(ProtocolWithSegmentation):
    ALLOWED_COMMANDS = (CREDIT_CONTROL_COMMAND,)
    VALID_COMMANDS = (CREDIT_CONTROL_COMMAND,)

    @staticmethod
    def get_headers(data):
        v, l, flags, cmd = struct.unpack(DIAMETER_HEADERS, data[:8])
        cmd = (ord(cmd[0]) << 16) | \
              (ord(cmd[1]) << 8) | \
              ord(cmd[2])
        l = (ord(l[0]) << 16) | \
            (ord(l[1]) << 8) | \
            ord(l[2])
        return v, l, flags, cmd


    @staticmethod
    def get_avp_value(diameter, avp_number):
        index = diameter.avps_index.get(avp_number, None)
        if index:
            if len(index) > 0:
                return diameter.avps[index[0]].value
            else:
                return None
        else:
            None

    def decode_ip(self, ip, timestamp):
        try:
            if len(ip.tcp.data) > 0:
                source_ip = socket.inet_ntoa(ip.src)
                destination_ip = socket.inet_ntoa(ip.dst)
                data = ip.tcp.data
                redis_key = None
                while data:
                    pkt_data = None
                    if len(data) > 8:
                        v, l, flags, cmd = self.get_headers(data)
                        if v == 1 and cmd in self.VALID_COMMANDS:  # Es el principio de un paquete
                            if l == len(data):  # Hay un solo paquete
                                pkt_data = data[:l]
                                data = None
                                redis_key = None
                            else:
                                if l < len(data):  # hay mas de un paquete
                                    pkt_data = data[:l]
                                    data = data[l:]
                                else:  # el paquete esta incompleto, es el principio de un paquete
                                    if not redis_key:
                                        redis_key = "%s:%s:%s" % (
                                            ip.tcp.sport, ip.tcp.dport, ip.tcp.seq + len(ip.tcp.data))
                                    tmp_redis_key = "%s.%d" % (redis_key, random.randint(1, 9999))
                                    try:
                                        self.redis_cache.rename(redis_key, tmp_redis_key)
                                        next_fragment = self.redis_cache.get(tmp_redis_key)
                                        self.redis_cache.delete(tmp_redis_key)
                                        #logger.debug("next fragment already on redis key(%s)", redis_key)
                                        next_fragment_data = pickle.loads(next_fragment)
                                        data = data + next_fragment_data['data']
                                        redis_key = next_fragment_data['next']
                                    except:
                                        self.redis_cache.set(redis_key, pickle.dumps({'data': data, 'next': None}))
                                        #logger.debug("Saving fragment key(%s)", redis_key)
                                        data = None
                                        redis_key = None
                        else:  # Es un fragmento que no empieza por el principio
                            redis_key = "%s:%s:%s" % (ip.tcp.sport, ip.tcp.dport, ip.tcp.seq)
                            time.sleep(0.01)
                            tmp_redis_key = "%s.%d" % (redis_key, random.randint(1, 9999))
                            try:
                                self.redis_cache.rename(redis_key, tmp_redis_key)
                                previous_fragment = self.redis_cache.get(tmp_redis_key)
                                self.redis_cache.delete(tmp_redis_key)
                                previous_fragment_data = pickle.loads(previous_fragment)
                                data = previous_fragment_data['data'] + data
                                #logger.debug("Previous fragment found (%s)", redis_key)

                            except:  # No Tengo el fragmento anterior, lo meto en redis
                                next_redis_key = "%s:%s:%s" % (
                                    ip.tcp.sport, ip.tcp.dport, ip.tcp.seq + len(ip.tcp.data))
                                self.redis_cache.set(redis_key, pickle.dumps({'data': data, 'next': next_redis_key}))
                                #logger.debug("Missing previous fragment, reprocess (key: %s)", redis_key)
                                data = None
                            redis_key = None
                    else:
                        data = None
                    if pkt_data:
                        try:
                            diameter_pkt = dpkt.diameter.Diameter(pkt_data)
                            if diameter_pkt.cmd in self.ALLOWED_COMMANDS:
                                diameter_pkt.timestamp = float(timestamp)
                                diameter_pkt.source_ip = source_ip
                                diameter_pkt.destination_ip = destination_ip
                                yield self.packet_to_tuple(diameter_pkt)
                            else:
                                del diameter_pkt
                        except Exception as e:
                            logger.error("Decode: %s (%s)", e, pkt_data.encode('hex'))
        except Exception as e:
            logger.error("Decode: Unknown Exception [%s]", e)

class NginDiameter(Diameter):
    CCR_TABLE = "ngin_creditcontrol_request"
    CCA_TABLE = "ngin_creditcontrol_answer"

    def packet_to_tuple(self, packet):
        if packet.request_flag:
            # CCR
            # timestamp, source_ip, destination_ip, request_type, termination_cause, subscription_id, origin_host
            # realm, session_id, orig_location_number, accumulated_time, called_party_number, traffic_case, traffic_type
            traffic_type = 1 if 102 in packet.avps_index else 0
            # Traffic case real, despues hay que poner el viejo traffic ase como traffic type: self.get_avp_value(packet, 42)
            packet_tuple = (datetime.fromtimestamp(packet.timestamp), packet.source_ip, packet.destination_ip,
                            self.get_avp_value(packet, 416), self.get_avp_value(packet, 295),
                            self.get_avp_value(packet, 444), self.get_avp_value(packet, 264),
                            self.get_avp_value(packet, 296), self.get_avp_value(packet, 263), self.format_cell(self.get_avp_value(packet, 21)),
                            self.get_avp_value(packet, 30), self.get_avp_value(packet, 9)[:50], self.get_avp_value(packet, 42), traffic_type)
            table = self.CCR_TABLE
        else:
            # CCA
            # timestamp, source_ip, destination_ip, request_type, result_code, origin_host, realm
            # session_id, release_cause, accumulated_time, announcement
            packet_tuple = (datetime.fromtimestamp(packet.timestamp), packet.source_ip, packet.destination_ip,
                            self.get_avp_value(packet, 416), self.get_avp_value(packet, 268),
                            self.get_avp_value(packet, 264), self.get_avp_value(packet, 296),
                            self.get_avp_value(packet, 263), self.get_avp_value(packet, 25),
                            self.get_avp_value(packet, 30), self.get_avp_value(packet, 3))
            table = self.CCA_TABLE
        return table, packet_tuple

    @staticmethod
    def format_cell(value):
        if value:
            try:
                if len(value) == 26:
                    swapped_data = ''.join([c[1] + c[0] for c in zip(value[::2], value[1::2])])
                    return swapped_data[11:-1]
                else:
                    return value
            except:
                return value
        else:
            return value


class DraDiameter(Diameter):
    CCR_TABLE = "dra_creditcontrol_request"
    CCA_TABLE = "dra_creditcontrol_answer"
    RAR_TABLE = "dra_reauth_request"
    RAA_TABLE = "dra_reauth_answer"
    ALLOWED_COMMANDS = (CREDIT_CONTROL_COMMAND, RE_AUTH_COMMAND)
    VALID_COMMANDS = (CREDIT_CONTROL_COMMAND, RE_AUTH_COMMAND)

    def packet_to_tuple(self, packet):
        if packet.cmd == CREDIT_CONTROL_COMMAND:
            if packet.request_flag:
                # CCR
                # timestamp, source_ip, destination_ip, request_type, termination_cause, subscription_id, max_bitrate_dl, origin_host, destination_host, session_id
                subscription_id_avps = packet.avps_index.get(444)
                if subscription_id_avps:
                    subscription_id = map(lambda x: x.value, filter(lambda x: x.value.startswith('54'), [packet.avps[i] for i in packet.avps_index.get(444)]))
                    if subscription_id:
                        subscription_id = subscription_id[0]
                    else:
                        subscription_id = None
                else:
                    subscription_id = None
                packet_tuple = (datetime.fromtimestamp(packet.timestamp), packet.source_ip, packet.destination_ip,
                                self.get_avp_value(packet, 416), self.get_avp_value(packet, 295),
                                subscription_id, self.get_avp_value(packet, 1040), self.get_avp_value(packet, 264),
                                self.get_avp_value(packet, 293), self.get_avp_value(packet, 263))
                table = self.CCR_TABLE
            else:
                # CCA
                # timestamp, source_ip, destination_ip, request_type, result_code, origin_host, session_id
                packet_tuple = (datetime.fromtimestamp(packet.timestamp), packet.source_ip, packet.destination_ip,
                                self.get_avp_value(packet, 416), self.get_avp_value(packet, 268),
                                self.get_avp_value(packet, 264), self.get_avp_value(packet, 263))
                table = self.CCA_TABLE
        elif packet.cmd == RE_AUTH_COMMAND:
            if packet.request_flag:
                # RAR
                # timestamp, source_ip, destination_ip, origin_host, destination_host, session_id
                packet_tuple = (datetime.fromtimestamp(packet.timestamp), packet.source_ip, packet.destination_ip,
                                self.get_avp_value(packet, 264), self.get_avp_value(packet, 293),
                                self.get_avp_value(packet, 263))
                table = self.RAR_TABLE
            else:
                # RAA
                # timestamp, source_ip, destination_ip, result_code, origin_host, session_id
                packet_tuple = (datetime.fromtimestamp(packet.timestamp), packet.source_ip, packet.destination_ip,
                                self.get_avp_value(packet, 268), self.get_avp_value(packet, 264),
                                self.get_avp_value(packet, 263))
                table = self.RAA_TABLE
        return table, packet_tuple


class SmppDiameter(Diameter):
    CCR_TABLE = "mco_creditcontrol_request"
    CCA_TABLE = "mco_creditcontrol_answer"

    def packet_to_tuple(self, packet):
        if packet.request_flag:
            # CCR
            # timestamp, source_ip, destination_ip, subscription_id, recipient_address, session_id
            packet_tuple = (datetime.fromtimestamp(packet.timestamp), packet.source_ip, packet.destination_ip,
                            self.get_avp_value(packet, 444), self.get_avp_value(packet, 897),
                            self.get_avp_value(packet, 263))
            table = self.CCR_TABLE
        else:
            # CCA
            # timestamp, source_ip, destination_ip, result_code, session_id
            packet_tuple = (datetime.fromtimestamp(packet.timestamp), packet.source_ip, packet.destination_ip,
                            self.get_avp_value(packet, 268), self.get_avp_value(packet, 263))
            table = self.CCA_TABLE
        return table, packet_tuple
