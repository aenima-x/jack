# -*- coding: utf-8 -*-
import logging
import socket
import struct
from datetime import datetime
import re

import dpkt

from jack.protocols.protocol import Protocol, ProtocolWithSegmentation, IP_PROTO_UDP, UDP_HEADER_STRUCT

logger = logging.getLogger(__name__)

re_pattern = re.compile(r".*<(.*)>.*")
re_content_length = re.compile(r".*(Content-Length\D*\s*\d*).*")
re_ani = re.compile(r"sip:\+?(\d{3,}).*")


def get_ani(ani_string):
    result = re_ani.search(ani_string)
    if result:
        ani = result.group(1)[:70]
    else:
        match = re_pattern.match(ani_string)
        if match:
            ani = match.group(1)[:70]
        else:
            ani = ani_string
    return ani


def is_packet(ip_data, protocol, src, dst, filter_data):
    if protocol == IP_PROTO_UDP:
        sport, dport, ulen, sum = struct.unpack(UDP_HEADER_STRUCT, ip_data[:8])
        protocol_port = filter_data.get("port", None)
        if protocol_port:
            if protocol_port in (sport, dport):
                protocol_ips = filter_data.get("ips", None)
                if protocol_ips:  # this protocol must match one of these ips
                    for ip_address in (socket.inet_ntoa(src), socket.inet_ntoa(dst)):
                        if ip_address in protocol_ips:
                            return True
                else:  # this protocol on must match port only
                    return True
        else:  # this protocol on must match protocol only
            return True
    return False

def get_sip_extra_info(packet):
    pass

class SIP(Protocol):

    def packet_to_tuple(self, packet):
        timestamp = datetime.fromtimestamp(packet.timestamp)
        from_number = get_ani(packet.headers['from'])
        to_number = get_ani(packet.headers['to'])
        call_id = packet.headers.get('call-id')
        if isinstance(packet, dpkt.sip.Request):
            table = self.REQUEST_TABLE
            # timestmap, source_ip, destination_ip, method, uri, from_number, to_number, call_id
            packet_tuple = (timestamp, packet.source_ip, packet.destination_ip, packet.method, packet.uri[:200], from_number, to_number, call_id)
        else:
            table = self.RESPONSE_TABLE
            # timestmap, source_ip, destination_ip, status_code, from_number, to_number, call_id, reason
            packet_tuple = (timestamp, packet.source_ip, packet.destination_ip, packet.status, from_number, to_number, call_id,
                            packet.reason)
        return table, packet_tuple

    def decode_ip(self, ip, timestamp):
        try:
            udp = ip.data
            try:
                if not udp.data.startswith('disabled'):
                    if ip.mf:
                        content_length_match = re_content_length.search(udp.data)
                        if content_length_match:
                            content_length_string = content_length_match.group(1)
                            udp.data = udp.data.replace(content_length_string, "")
                    if udp.data[:3] == 'SIP':
                        # Response
                        sip_packet = dpkt.sip.Response(udp.data)
                    else:
                        # Request
                        sip_packet = dpkt.sip.Request(udp.data)
                    sip_packet.timestamp = float(timestamp)
                    sip_packet.source_ip = socket.inet_ntoa(ip.src)
                    sip_packet.destination_ip = socket.inet_ntoa(ip.dst)
                    sip_packet.source_port = udp.sport
                    sip_packet.destination_port = udp.dport
                    yield self.packet_to_tuple(sip_packet)
            except Exception as e:
                logger.error("Decode: %s (%s)", e, udp.data.encode('hex'))
        except Exception as e:
            logger.error("Decode: Unknown Exception [%s]", e)


# class Tugo(SIP):
#
#     REQUEST_TABLE = "tugo_request"
#     RESPONSE_TABLE = "tugo_response"


class Volte(SIP):

    REQUEST_TABLE = "volte_request"
    RESPONSE_TABLE = "volte_response"

    def packet_to_tuple(self, packet):
        timestamp = datetime.fromtimestamp(packet.timestamp)
        from_number = get_ani(packet.headers['from'])
        to_number = get_ani(packet.headers['to'])
        call_id = packet.headers.get('call-id')
        if isinstance(packet, dpkt.sip.Request):
            network = ""
            network_info = packet.headers.get('p-access-network-info', None)
            if network_info:
                if network_info.startswith('IEEE-802.11'):
                    network = 'wifi'
                else:
                    network = '4G'
            table = self.REQUEST_TABLE
            # timestmap, source_ip, destination_ip, method, uri, from_number, to_number, call_id, network
            packet_tuple = (timestamp, packet.source_ip, packet.destination_ip, packet.method, packet.uri[:200], from_number, to_number, call_id, network)
        else:
            table = self.RESPONSE_TABLE
            # timestmap, source_ip, destination_ip, status_code, from_number, to_number, call_id, reason
            packet_tuple = (timestamp, packet.source_ip, packet.destination_ip, packet.status, from_number, to_number, call_id, packet.reason)
        return table, packet_tuple

class VPNFija(SIP):

    REQUEST_TABLE = "vpn_fija_request"
    RESPONSE_TABLE = "vpn_fija_response"

