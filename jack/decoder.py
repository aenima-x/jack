# -*- coding: utf-8 -*-
import logging
import multiprocessing
from collections import namedtuple
import importlib

from jack.utils import RedisConfig, DBConfig, RedisCacheConfig

# Protocol Decoders

logger = logging.getLogger(__name__)

Protocol = namedtuple("Protocol", ("name", "module", "protocol", "input_queue", "workers", "buffer_size", "db_config",
                                   "redis_input_config", "redis_cache_config"))


class DecoderManager(object):

    def __init__(self, config):
        super(DecoderManager, self).__init__()
        self.protocols = []
        self.load_config(config)

    def load_config(self, config):
        for protocol_data in config["protocols"]:
            protocol_data["redis_input_config"] = RedisConfig(**config["redis_input_config"])
            if "redis_cache_config" in config:
                protocol_data["redis_cache_config"] = RedisCacheConfig(**config["redis_cache_config"])
            else:
                protocol_data["redis_cache_config"] = None
            protocol_data["db_config"] = DBConfig(**config["db_config"])
            self.protocols.append(Protocol(**protocol_data))

    def protocol_factory(self, protocol_dto):
        # get decoder class
        module = importlib.import_module("jack.protocols.%s" % protocol_dto.module)
        _class = getattr(module, protocol_dto.protocol)
        return _class

    def launch_protocol_decoder(self, protocol_decoder_class, protocol_dto):
        protocol_decoder = protocol_decoder_class(protocol_dto)
        protocol_decoder.run()

    def run(self):
        logger.debug("creating workers")
        for protocol_dto in self.protocols:
            for i in range(protocol_dto.workers):
                protocol_decoder_class = self.protocol_factory(protocol_dto)
                p = multiprocessing.Process(target=self.launch_protocol_decoder,
                                            args=(protocol_decoder_class, protocol_dto),
                                            name="decoder-%s-%d" % (protocol_dto.name, i))
                p.start()
