#!/usr/bin/env bash
JACK_LAUNCHER="`dirname $0`/jack_launcher.py"

if [ $(ps -ef |grep python |grep -c jack_launcher.py) -gt 1 ];then
    echo "Ya esta corriendo"
else
    echo "Start Jack"
    nohup sudo $JACK_LAUNCHER --sniffer --dispatcher --decoder &
fi
