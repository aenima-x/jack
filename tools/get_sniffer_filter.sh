#!/bin/bash
#export expression="sctp or port 3865 or port 5060";
if [ $# -ne 1 ]; then
    echo "Usage $0 \"filter\""
else
    export expression=$1
    paste -d"\n" \
    <(sudo tcpdump -ni eno2 -d $expression | sed -e 's/^/# /') \
    <(sudo tcpdump -ni eno2 -dd $expression |tr -s '{}' '[]') |egrep -v "^#"| python -c "import sys; print(eval('[%s]' % sys.stdin.read().replace('\n', '')))"
fi
