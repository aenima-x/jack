#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import sys
import time
from datetime import datetime

import redis

# Parameters

parser = argparse.ArgumentParser(description='Jack: Redis Queue Monitor.')
parser.add_argument("-host", type=str, default="localhost", help='Redis host')
parser.add_argument("-port", type=int, default=6379, help='Redis port')
parser.add_argument("-r", "--refresh", type=int, default=5, help='Set the refresh time')



def main(args):
    redis_server = redis.Redis(host=args.host, port=args.port)
    print_wait = args.refresh
    print("Jack Queues: (refresh every %s sec)" % print_wait)
    while True:
        try:
            redis_counter = {x: {'name': x.split(":")[1], 'count': redis_server.llen(x)} for x in redis_server.keys()}
            sys.stdout.write("%s - Queues: [%s]\r" % (datetime.now(), ",".join(map(lambda x: "%s:%d" % (x['name'], x['count']), redis_counter.values()))))
            sys.stdout.flush()
            time.sleep(print_wait)
        except KeyboardInterrupt as e:
            raise e
        except:
            pass



if __name__ == "__main__":
    try:
        args = parser.parse_args()
        main(args)
    except KeyboardInterrupt:
        print("")
    except Exception as e:
        print(e)
