#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import logging
import os
import sys
import json

if os.getuid() != 0:
    print("Must run as root!!")
    sys.exit(1)


BASE_DIR = os.path.dirname(__file__)

# log levels
log_levels = {"DEBUG": logging.DEBUG,
              "INFO": logging.INFO,
              "WARNING": logging.WARNING,
              "ERROR": logging.ERROR,
              "CRITICAL": logging.CRITICAL}

# Parameters

parser = argparse.ArgumentParser(description='Jack The Ripper.')
parser.add_argument('--sniffer', dest='run_sniffer', action='store_true', help='Run the Sniffer')
parser.add_argument('--dispatcher', dest='run_dispatcher', action='store_true', help='Run the Dispatcher')
parser.add_argument('--decoder', dest='run_decoder', action='store_true', help='Run the Decoder')
parser.add_argument('-v', dest='verbose', action='store_true', help='Run int verbose mode')
args = parser.parse_args()


# load global config
CONFIG_PATH = os.path.join(BASE_DIR, "config")
LAUNCHER_CONFIG_FILE = os.path.join(CONFIG_PATH, "launcher.json")
if not os.path.isfile(LAUNCHER_CONFIG_FILE):
    print("Missing config file (%s)" % LAUNCHER_CONFIG_FILE)
    sys.exit(1)
else:
    try:
        with open(LAUNCHER_CONFIG_FILE) as f:
            launcher_config = json.load(f)
    except Exception as e:
        print(e)
        sys.exit(1)

# Setup Logger
logger = logging.getLogger("jack")
FORMAT = '%(asctime)-15s: - %(module)s(%(processName)s) [%(levelname)s] - %(message)s'
LOG_LEVEL = log_levels[launcher_config['log_level']]
formatter = logging.Formatter(FORMAT)
LOG_PATH = os.path.join(BASE_DIR, "logs")
if not os.path.isdir(LOG_PATH):
    os.mkdir(LOG_PATH)
LOG_FILE = os.path.join(LOG_PATH, "jack.log")
handler = logging.FileHandler(LOG_FILE)
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(LOG_LEVEL)
if args.verbose:
    handler = logging.StreamHandler()
    handler.setFormatter(formatter)
    logger.addHandler(handler)

from jack.sniffer import SnifferManager
from jack.dispatcher import DispatcherManager
from jack.decoder import DecoderManager

# Config files

SNIFFER_CONFIG_FILE = "sniffer.json"
DISPATCHER_CONFIG_FILE = "dispatcher.json"
DECODER_CONFIG_FILE = "decoder.json"


def main(args):
    logger.info("Start Jack [pid:%d]", os.getpid())
    selected_options = filter(lambda x: x[0], [(args.run_sniffer, SnifferManager, SNIFFER_CONFIG_FILE),
                                               (args.run_dispatcher, DispatcherManager, DISPATCHER_CONFIG_FILE),
                                               (args.run_decoder, DecoderManager, DECODER_CONFIG_FILE)])
    if not selected_options:
        parser.print_help()
    else:
        CONFIG_PATH = os.path.join(BASE_DIR, "config")
        if not os.path.isdir(CONFIG_PATH):
            logger.error("Config Path Missing (%s)", CONFIG_PATH)
            return
        for _, class_name, config_file in selected_options:
            logger.info("Create: %s (%s)", class_name.__name__, config_file)
            config_file_path = os.path.join(CONFIG_PATH, config_file)
            if not os.path.isfile(config_file_path):
                logger.error("Config File (%s) Missing", config_file_path)
                return
            else:
                with open(config_file_path) as f:
                    module_config_json = json.load(f)
                    module_object = class_name(module_config_json)
                    module_object.run()
        logger.info("enter run loop")
        while True:
            pass


if __name__ == "__main__":
    try:
        main(args)
    except KeyboardInterrupt:
        pass
    except Exception as e:
        logger.exception(e)


